import java.util.*;
public class OXgame2_New {

	private static void printWelcome() {
		System.out.println("Welcome to OX game");
		
	}
	static String ox[][] = {{" ","1","2","3"},{"1","-","-","-"},{"2","-","-","-"},{"3","-","-","-"}};
	private static void printTable() {
		for(int i=0;i<4;i++) {
			for(int j=0;j<4;j++) {
				System.out.print(ox[i][j]+" ");	
			}
			System.out.println();
		}
		
	}
	static String turn = "x";
	private static void printTurn() {
		System.out.println("Turn "+turn);
	}
	static int r,c;
	private static void inputPosition() {
		Scanner scan = new Scanner(System.in);
		System.out.println("Please input Row,Col:");
		String row=scan.next();
		
		if(row.equals("1")) {
			r=1;
		}else if(row.equals("2")){
			r=2;
		}else if(row.equals("3")){
			r=3;
		}else{
			System.out.println("Please enter number 1-3.");
			printTurn();
			inputPosition();
		}
		String col=scan.next();
		if(col.equals("1")) {
			c=1;
		}else if(col.equals("2")) {
			c=2;
		}else if(col.equals("3")) {
			c=3;
		}else {
			System.out.println("Please enter number 1-3.");
			printTurn();
			inputPosition();
		}
	}
	static int count = 0;
	private static void checkPosition() {
		if(turn.equals("x")) {
			if(ox[r][c].equals("-")) {
				ox[r][c]="x";
				count++;
			}else if(ox[r][c].equals("x")||ox[r][c].equals("o")) {
				System.out.println("That position is already used.");
				printTurn();
				inputPosition();
				checkPosition();
			}
		}else if(turn.equals("o")){
			if(ox[r][c].equals("-")) {
				ox[r][c]="o";
				count++;
			}else if(ox[r][c].equals("x")||ox[r][c].equals("o")) {
				System.out.println("That position is already used.");
				printTurn();
				inputPosition();
				checkPosition();
			}
		}
		
	}
	static String w = "";
	private static void checkResult() {
		if(ox[1][1].equals(turn)&&ox[1][2].equals(turn)&&ox[1][3].equals(turn)) {
			w=turn+" win!!";
		}else if(ox[2][1].equals(turn)&&ox[2][2].equals(turn)&&ox[2][3].equals(turn)) {
			w=turn+" win!!";
		}else if(ox[3][1].equals(turn)&&ox[3][2].equals(turn)&&ox[3][3].equals(turn)) {
			w=turn+" win!!";
		}else if(ox[1][1].equals(turn)&&ox[2][1].equals(turn)&&ox[3][1].equals(turn)) {
			w=turn+" win!!";
		}else if(ox[1][2].equals(turn)&&ox[2][2].equals(turn)&&ox[3][2].equals(turn)) {
			w=turn+" win!!";
		}else if(ox[1][3].equals(turn)&&ox[2][3].equals(turn)&&ox[3][3].equals(turn)) {
			w=turn+" win!!";
		}else if(ox[1][1].equals(turn)&&ox[2][2].equals(turn)&&ox[3][3].equals(turn)) {
			w=turn+" win!!";
		}else if(ox[1][3].equals(turn)&&ox[2][2].equals(turn)&&ox[3][1].equals(turn)) {
			w=turn+" win!!";
		}
		else if(count == 9) {
			w = "Draw!!";
		}
		
	}
	private static void printResult() {
		System.out.println(w);
		
	}
	private static void switchTurn() {
		if(turn.equals("x")) {
			turn="o";
		}else {
			turn="x";
		}
	}
	public static void main(String[] args) {
		
		printWelcome();
		
		for(;;) {
			printTable();
			printTurn();
			inputPosition();
			checkPosition();
			checkResult();
			switchTurn();
			if(w.equals("x win!!")) {
				break;
			}else if(w.equals("o win!!")) {
				break;
			}else if(w.equals("Draw!!")) {
				break;
			}
		}
		printTable();
		printResult();

	}
	
}
